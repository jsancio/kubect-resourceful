extern crate clap;
extern crate failure;
extern crate k8s_openapi;
extern crate kubectl_resourceful;
extern crate kubernetes;

use k8s_openapi::v1_12::api::core::v1;
use kubectl_resourceful::config::Config;
use kubectl_resourceful::error::AppError;
use kubectl_resourceful::ior::Ior;
use kubernetes::client::APIClient;
use kubernetes::config;
use std::process::Command;


fn main() -> Result<(), AppError> {
    let config = Config::new()?;

    let kubeconfig = config::load_kube_config()?;
    let kubeclient = APIClient::new(kubeconfig);

    let req =
        v1::Pod::read_core_v1_namespaced_pod_status(&config.pod_name, &config.namespace, None)?;

    let pod = kubeclient.request::<v1::Pod>(req)?;

    let host_ip = match pod.status {
        Some(v1::PodStatus {
            host_ip: Some(host_ip),
            ..
        }) => host_ip,
        _ => {
            return Err(AppError::GenericError(String::from(
                "unable to find host ip for pod",
            )));
        }
    };


    let command_output = Command::new("ssh")
        .arg("-T")
        .arg(host_ip)
        .arg(docker_command(&config.container, &config.flag))
        .output()?;

    println!("{}", String::from_utf8_lossy(&command_output.stdout));

    Ok(())
}

fn docker_command(container: &str, memory_or_cpus: &Ior<usize, f64>) -> String {
    let flags = match memory_or_cpus {
        Ior::Left(memory) => format!("--memory {} ", memory),
        Ior::Right(cpus) => format!("--cpu-period {} ", cpus),
        Ior::Both(memory, cpus) => format!("--memory {} --cpu-period {} ", memory, cpus),
    };

    let mut command = String::from("docker container update ");
    command.push_str(&flags);
    command.push_str(container);

    command
}
