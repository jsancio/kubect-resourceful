extern crate clap;

use self::clap::App;
use self::clap::Arg;
use self::clap::ArgMatches;
use crate::error::AppError;
use crate::ior::Ior;

pub struct Config {
    pub namespace: String,
    pub pod_name: String,
    pub container: String,
    pub flag: Ior<usize, f64>,
}

static NAMESPACE_KEY: &str = "NAMESPACE";
static POD_KEY: &str = "POD";
static CONTAINER_KEY: &str = "CONTAINER";
static CPU_KEY: &str = "CPU";
static MEMORY_KEY: &str = "MEMORY";

impl Config {
    pub fn new() -> Result<Config, AppError> {
        let matches = Config::create_matches();

        let namespace = String::from(matches.value_of(NAMESPACE_KEY).unwrap_or("default"));
        let pod_name = String::from(matches.value_of(POD_KEY).unwrap());
        let container = String::from(matches.value_of(CONTAINER_KEY).unwrap());

        let memory = match matches.value_of(MEMORY_KEY) {
            Some(value) => Some(str::parse::<usize>(value)?),
            None => None,
        };

        let cpus = match matches.value_of(CPU_KEY) {
            Some(value) => Some(str::parse::<f64>(value)?),
            None => None,
        };

        let flag = Ior::new(memory, cpus).ok_or(AppError::GenericError(String::from(
            "must specify either --cpu-period or --memory",
        )))?;

        Ok(Config {
            namespace,
            pod_name,
            container,
            flag,
        })
    }

    fn create_matches() -> ArgMatches<'static> {
        App::new("kubectl-resourceful")
        .arg(
            Arg::with_name(NAMESPACE_KEY)
            .long("namespace")
            .value_name("NAMESPACE")
            .help("Kubernetes Namespace resource name.")
            .takes_value(true)
        ).arg(
            Arg::with_name(POD_KEY)
            .long("pod")
            .value_name("POD")
            .help("Kubernetes Pod resource name.")
            .takes_value(true)
            .required(true)
        ).arg(
            Arg::with_name(CONTAINER_KEY)
            .long("container")
            .value_name("CONTAINER")
            .help("Kubernetes Container resource name.")
            .takes_value(true)
            .required(true)
        ).arg(
            Arg::with_name(CPU_KEY)
            .long("cpu-period")
            .value_name("CPU")
            .help("Specify the CPU CFS scheduler period. Defaults to 100 micro-seconds.")
            .takes_value(true),
        ).arg(
            Arg::with_name(MEMORY_KEY)
            .long("memory")
            .value_name("MEMORY")
            .help("The maximum amount of memory the container can use. If you set this option, the minimum allowed value is 4m (4 megabyte).")
            .takes_value(true)
        ).get_matches()
    }
}
