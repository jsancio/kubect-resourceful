pub enum Ior<A, B> {
    Left(A),
    Right(B),
    Both(A, B),
}

impl<A, B> Ior<A, B> {
    pub fn new(left: Option<A>, right: Option<B>) -> Option<Ior<A, B>> {
        match (left, right) {
            (Some(left), Some(right)) => Some(Ior::Both(left, right)),
            (Some(left), None) => Some(Ior::Left(left)),
            (None, Some(right)) => Some(Ior::Right(right)),
            (None, None) => None,
        }
    }
}
