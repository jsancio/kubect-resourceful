extern crate k8s_openapi;

use self::k8s_openapi::RequestError;
use std::io;
use std::num::ParseFloatError;
use std::num::ParseIntError;

#[derive(Debug)]
pub enum AppError {
    ParseIntError(ParseIntError),
    ParseFloatError(ParseFloatError),
    IoError(io::Error),
    GenericError(String),
    Failure(failure::Error),
    RequestError(RequestError),
}

impl From<ParseIntError> for AppError {
    fn from(error: ParseIntError) -> AppError {
        AppError::ParseIntError(error)
    }
}

impl From<ParseFloatError> for AppError {
    fn from(error: ParseFloatError) -> AppError {
        AppError::ParseFloatError(error)
    }
}

impl From<io::Error> for AppError {
    fn from(error: io::Error) -> AppError {
        AppError::IoError(error)
    }
}

impl From<failure::Error> for AppError {
    fn from(error: failure::Error) -> AppError {
        AppError::Failure(error)
    }
}

impl From<RequestError> for AppError {
    fn from(error: RequestError) -> AppError {
        AppError::RequestError(error)
    }
}
