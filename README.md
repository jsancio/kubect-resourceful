# Kubectl Resourceful

## Motivation
In a Kubernetes cluster, you have long-running stateful services using local SSD (like TiDB!). You want to dynamically make it possible for a container to adjust (up or down) its resource usage limits to be different from its current limit without interrupting availability of the service.

## Requirements
Implement a kubectl plugin that allows a container to access more resources than its current resource limit set in k8s, so these two commands can run:

```
kubectl plugin resourceful --cpu-period 500000 --namespace <ns> --pod <pod-name> --container <container-name>
kubectl plugin resourceful --memory=2147484000 --namespace <ns>  --pod <pod-name> --container <container-name>
```
Notice the slight changes to the command line from the original description.

This should be dynamic: the container should not be restarted.
It should also be safe in that if the resource limit is moved upward, the container should not get killed for going over its original k8s limit.

## Kubernetes Architecture
Before outlining the solution to this problem we must first understand some part of the kubernetes architecture. Container managed by a kubernetes run in kubernetes nodes. A typical kubernetes node has two processes running: kubelet and docker.

Docker has support for resizing the memory and cpu period[1][2]. The kubelet CRI runtime API supports updating container information but that doesn't included memory and CPU limits[3]. This means that the plugin needs to make changes directly to the Docker container and bypass Kubernetes/kubelet. Unfortunately, docker does not listen to TCP connections by default. It instead listens on a UNIX domain socket.

## Solutions
Given the requirements and restriction listed above it is clear that any solution would have to execute plugin specific code on the node. On a typical Kubernetes cluster there are two ways to execute code on a node: a container using kubelet and docker or SSH command.

### Docker Container
At a high level this solution would entail a docker image that once executed would write to the Docker UNIX domain socket with the request to resize the memory and CPU. This solution was not implemented because it would require creating a docker image with the algorithm describe above and publishing it to the Docker Registry configured for the cluster. I don't that a solution that required access to the cluster's registry was shippable to the typical users.

### SSH Command
At a high level this solution discovers the host ip where the container is running and executes a docker command through SSH to resize the container's memory and CPU period.

## Example Usage

```
$ kubectl describe pod nginx-deployment-6d7c766476-zj92l
Name:               nginx-deployment-6d7c766476-zj92l
Namespace:          default
Node:               worker-1/10.0.2.5
...

$ kubectl resourceful --pod nginx-deployment-6d7c766476-zj92l --container 5fcdaaf0795a -
-memory 1234567890 --cpu-period 10000
5fcdaaf0795a
Your kernel does not support swap limit capabilities or the cgroup is not mounted. Memory limited without swap.

$ ssh -T 10.0.2.5 docker container inspect 5f
...
            "CpuShares": 2,
            "Memory": 1234567890,
            "NanoCpus": 0,
            "CgroupParent": "/kubepods/besteffort/podc4bae4af-e88e-11e8-b367-0800273b40e2",
            "BlkioWeight": 0,
            "BlkioWeightDevice": null,
            "BlkioDeviceReadBps": null,
            "BlkioDeviceWriteBps": null,
            "BlkioDeviceReadIOps": null,
            "BlkioDeviceWriteIOps": null,
            "CpuPeriod": 10000,
            "CpuQuota": 0,
            "CpuRealtimePeriod": 0,
            "CpuRealtimeRuntime": 0,
            "CpusetCpus": "",
            "CpusetMems": "",
...
```

## Limitations
### Requires SSH
The implementation requires SSH access. Even though most Linux installations have SSHd this is not strictly required by Kubernetes.

### Assumes Docker
The implementation assumes that Kubernetes has been configured to use Docker. This is not strictly required by Kubernetes. The kubelet has abstracted this component but that abstraction doesn't support updating the configuration for memory and CPU periods. If Kubernetes is configured to use a different containerizer (e.g. rkt) then a final solution would need to abstract the different implementations.

### Doesn't tolerate deployment or pod changes
The implementation makes changes directly to the Docker container bypassing Kubernetes' API server. As such any changes to the deployment or runtime that results in the pod/container getting re-executed would lead to the change being overridden.

### Required permission
The user used to execute the SSHed docker command must have access to write and read to Docker's UNIX domain socket. By default the permission looks as follow:

```
$ ls -l /var/run/docker.sock
srw-rw---- 1 root docker 0 Nov 17 22:32 /var/run/docker.sock
```

## References
1. [https://docs.docker.com/config/containers/resource_constraints/](https://docs.docker.com/config/containers/resource_constraints/)
1. [https://docs.docker.com/engine/api/v1.39/#operation/ContainerUpdate](https://docs.docker.com/engine/api/v1.39/#operation/ContainerUpdate)
Some notes to use for the feature explanation
1. [https://github.com/kubernetes/kubernetes/blob/master/pkg/kubelet/apis/cri/runtime/v1alpha2/api.proto](https://github.com/kubernetes/kubernetes/blob/master/pkg/kubelet/apis/cri/runtime/v1alpha2/api.proto)
